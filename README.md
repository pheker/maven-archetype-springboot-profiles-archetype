# maven-archetype-springboot-profiles-archetype

#### 1.介绍
maven原型-即用于创建springboot多环境项目的模板

#### 2.使用方法：  
* （idea)创建项目,add archetype, 填写相关信息,使用新添加的archetype生成项目
* 之前安装到仓库(mvn clean install)一般会在仓库下生成了archetype-catalog.xml（如果没有则可手动生成mvn archetype:crawl）  
1.eclipse可添加此文件、生成项目时使用此项目生成即可  
2.也可以使用命令行生成项目：  
```cmd
mvn archetype:generate -DarchetypeCatalog=local
```

上面使用交互模式生成项目，如不使用交互模式的话：  
```cmd
mvn archetype:generate -DgroupId=com.alibaba -DartifactId=jackma -Dversion=1.0 -DarchetypeGroupId=cn.pheker -DarchetypeArtifactId=maven-archetype-springboot-profiles-archetype -DarchetypeVersion=1.0-SNAPSHOT -DinteractiveMode=false -DarchetypeCatalog=local

```

#### 3.参数说明
-DgroupId=					组织或公司名称如：com.google  
-DartifactId=				项目名称  
-Dpackage=					包名，如：com.google，默认为配置的groupId  
-Dversion=					版本，如1.0-SNAPSHOT此为默认  
-DarchetypeGroupId=         要使用的模板的组织名称  
-DarchetypeArtifactId=		要使用的模板的项目名称  
-Dversion=                  要使用的模板的版本  
-DinteractiveMode=false		不使用交互模式  
-DarchetypeCatalog=internal 使用内部的(remote、internal、local)原型目录而不使用maven中心的(默认remote原型过多可能导致卡死)  
