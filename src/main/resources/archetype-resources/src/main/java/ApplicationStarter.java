#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

/**
 * Hello world!
 * todo 因为没有数据源，所以先配置禁用数据源配置
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ImportResource(locations = "classpath*:spring*.xml")
public class ApplicationStarter {
    public static void main( String[] args ) {
        SpringApplication.run(ApplicationStarter.class, args);
    }
}
