# maven-archetype-springboot-profiles

## 一、自定义archetype
#### 介绍
SpringBoot项目多环境通用模板archetype  
1.基于SpringBoot  
2.多环境,dev-test-pre-pro  
3.是一个通用的模板项目,通用是指项目的结构通用  
4.用于生成自定义archetype

#### 项目结构
```
src
├─main
│  ├─filters
│  │      dev.properties
│  │      pre.properties
│  │      pro.properties
│  │      test.properties
│  │
│  ├─java
│  │  └─cn
│  │      └─pheker
│  │          │  ApplicationStarter.java
│  │          │
│  │          ├─common
│  │          │      IpUtils.java
│  │          │
│  │          └─modules
│  └─resources
│      │  application.properties
│      │  spring.xml
│      │
│      ├─doc
│      │      maven-archetype.txt
│      │
│      └─webapp
└─test
    ├─java
    │  └─cn
    │      └─pheker
    │              ApplicationStarterTest.java
    │
    └─resources
```


#### 反向生成模板-步骤
在根目录maven-archetype-springboot-profiles下执行mvn archetype:create-from-project进行反向生成原型,如果成功则会提示：
```
 Archetype project created in F:\workspace\learn-mvn\maven-archetype-springboot-profiles\target\generated-sources\archetype
```
这个名称为archetype的项目就是我们的原型项目,我们可以在此项目下mvn clean install将此项目安装到本地仓库(此时就可以在项目中使用了,创建项目,add archetype, 填写相关信息,使用新添加的archetype生成项目),但会发现生成的项目结构与我们预期的不是很一样：1.没有空文件夹。所以我们可以如下操作：  
1.pom.xml的buid中加入
```xml
<!-- 处理空文件夹 -->
<plugins>
  <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-resources-plugin</artifactId>
      <version>2.3</version>
      <configuration>
        <includeEmptyDirs>true</includeEmptyDirs>
      </configuration>
  </plugin>
</plugins>
```

2.修改archetype-metadata.xml
```text
<?xml version="1.0" encoding="UTF-8"?>
<archetype-descriptor xsi:schemaLocation="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.0.0 
    http://maven.apache.org/xsd/archetype-descriptor-1.0.0.xsd" name="maven-archetype-springboot-profiles"
    xmlns="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-descriptor/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <fileSets>
    <fileSet filtered="true" packaged="true" encoding="UTF-8">
      <directory>src/main/java</directory>
      <includes>
        <include>**/*.**</include>
      </includes>
    </fileSet>
    <fileSet filtered="true" encoding="UTF-8">
      <directory>src/main/resources</directory>
      <includes>
        <include>**/*.**</include>
      </includes>
    </fileSet>
    <fileSet filtered="true" encoding="UTF-8">
      <directory>src/main/filters</directory>
      <includes>
        <include>**/*.**</include>
      </includes>
    </fileSet>
    <fileSet filtered="true" packaged="true" encoding="UTF-8">
      <directory>src/test/java</directory>
      <includes>
        <include>**/*.**</include>
      </includes>
    </fileSet>
   
    <fileSet filtered="true" encoding="UTF-8">
        <directory>src/test/resources</directory>
        <includes>
             <include>**/*.**</include>
        </includes>
    </fileSet>
    
    <fileSet filtered="true" encoding="UTF-8">
        <directory>src/main/webapp</directory>
        <includes>
            <include>**/*.**</include>
        </includes>
    </fileSet>
  </fileSets>
</archetype-descriptor>
```
再执行mvn clean install (至于部署,可自行百度)     

## 模板现在已经在仓库中了,那如何通过此模板创建项目呢？
两种方法：  
* （idea)创建项目,add archetype, 填写相关信息,使用新添加的archetype生成项目
* 之前安装到仓库(mvn clean install)一般会在仓库下生成了archetype-catalog.xml（如果没有则可手动生成mvn archetype:crawl）  
1.eclipse可添加此文件、生成项目时使用此项目生成即可
2.也可以使用命令行生成项目：
```cmd
mvn archetype:generate -DarchetypeCatalog=local
```
上面使用交互模式生成项目，如不使用交互模式的话：  
```cmd
mvn archetype:generate -DgroupId=com.alibaba -DartifactId=jackma2 -Dversion=1.0 -DarchetypeGroupId=cn.pheker -DarchetypeArtifactId=maven-archetype-springboot-profiles-archetype -DarchetypeVersion=1.0-SNAPSHOT -DinteractiveMode=false -DarchetypeCatalog=local

```



## 二、maven创建项目-说明

#### 参数说明
-DgroupId=					组织或公司名称如：com.google
-DartifactId=				项目名称
-Dpackage=					包名，如：com.google，默认为配置的groupId
-Dversion=					版本，如1.0-SNAPSHOT此为默认
-DarchetypeGroupId=         要使用的模板的组织名称
-DarchetypeArtifactId=		要使用的模板的项目名称
-Dversion=                  要使用的模板的版本
-DinteractiveMode=false		不使用交互模式
-DarchetypeCatalog=internal 使用内部的(remote、internal、local)原型目录而不使用maven中心的(默认remote原型过多可能导致卡死)

#### 使用quickstart模板创建java项目
mvn archetype:generate -DgroupId=cn.pheker -DartifactId=learn-maven-quickstart -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false -DarchetypeCatalog=internal

```
目录结构：
   |-src
   |---main
   |-----java
   |-------cn
   |---------pheker   
   |-----------App.java
   |---test
   |-----java
   |-------cn
   |---------pheker
   |-----------AppTest.java
   |-pom.xml
```

POM默认信息：
```
  <modelVersion>4.0.0</modelVersion>
  <groupId>cn.pheker</groupId>
  <artifactId>learn-maven-quickstart2</artifactId>
  <packaging>jar</packaging>
  <version>1.0.0-dev</version>
  <name>learn-maven-quickstart2</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
```


#### 使用webapp模板创建web项目
mvn archetype:generate -DgroupId=cn.pheker -DartifactId=learn-maven-webapp -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false -DarchetypeCatalog=internal

```
|----pom.xml
|----src
|----main
|------resources
|------webapp
|--------index.jsp
|--------WEB-INF
|----------web.xml
```
可以看到相比于quickstart模板创建的项目，多了resources与webapp目录，少了java目录与测试目录

所以自己要手动增删目录或文件即可，如果我们想用一套自定义的通用模板，可以自定义archetype

#### 自定义archetype
方式有两种：  
1.按archetype规范手动创建原型项目并配置；  
2.利用现有项目反向生成原型项目，并可以修改配置进行定制化  

#### 如何删除IDEA中自己添加的archetype
${user.home}\.IntelliJIdea2018.3\system\Maven\Indices\UserArchetypes.xml
